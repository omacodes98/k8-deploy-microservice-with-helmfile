# Demo Projects - Deploy Microservices with Helmfile 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Deploy Microservices with Helm

* Deploy Microservices with Helmfile

## Technologies Used 

* Kubernetes 

* Helm 

* Helmfile 

## Steps 

Step 1: Youn can create a script that deploys all microservices through helm however this is not a good way of doing this as it will require a lot more effort.

[Created script](/images/01_create_a_script_to_run_all_microservices_and_redis_through_helm_charts.png)

Step 2: Create a helmfile as this is a better way of doing this 

[Helmfile](/images/02_create_helmfile.png)

Step 3: Install helmfile tool 

    brew install helmfile 

[Helm tool](/images/03_install_helmfile_tool.png)

Step 4: Deploy microservices and redis through helmfile 

    helmfile sync 

[Deploying microservices and redis](/images/04_deploying_microservices_and_redis_through_helmfile.png)

[Extre info](/images/05_extra_information.png)

Step 5: Check if application is working through browser 

[Application on Browser](/images/06_application_running_on_browser.png)

Step 6: When done you can stop the application 

    helmfile destroy 

[Uninstall all releases with one command](/images/07_uninstall_all_releases_with_one_command.png)

## Installation

Run $ brew install minikube 

## Usage 

    helmfile sync 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/k8-deploy-microservice-with-helmfile.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/k8-deploy-microservice-with-helmfile

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.